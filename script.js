const messages = [
  {
    id: 1,
    title: "Preciso assistir a demo?",
    text: "Sim, a demo é obrigatória, e todos os devs devem participar, caso não participem devem justificar a falta.",
  },
  {
    id: 2,
    title: "Preciso participar da daily?",
    text: "Sim, assim como a demo a daily é uma peça fundamental para o desenvolvimento dos devs e deve ser acommpanhada todos os dias, ausensia deve ser justificada.",
  },
  {
    id: 3,
    title: "Preciso participar da mesa redonda?",
    text: "Sim, a participação de todos os devs se faz necessaria durante a mesa redonda da kenzie.",
  },
  {
    id: 4,
    title: "Preciso participar das reuniões de fim de sprint?",
    text: "Sim, todas as reuniões de finais de sprint também são obrigatórias.",
  },
  {
    id: 13,
    title: "Preciso participar de talks?",
    text: "Sim, toda e qualquer chamada de video realizada pela equipe de ensino é obrigatória",
  },
  {
    id: 12,
    title: "O que é uma 1x1?",
    text: "São reuniões onde apenas você e outra pessoa vão participar, geralmente acontecem entre seu facilitador ou coachs, geralmente é para se conhecerem ou conversarem.",
  },
  {
    id: 12,
    title: "O que é o /pergunta?",
    text: "Sistema de perguntas que manda uma pergunta a um canal onde estão os facilitadores e os coachs, garantia de resposta no menor tempo possivel, basta mandar uma mensagem no canal do seu facilitador usando /pergunta seguido do titulo da sua pergunta e uma descrição da mesma. ",
  },
  {
    id: 12,
    title: "Como eu sei quais são meus coachs?",
    text: "Assim que forem distribuidos entre os alunos eles entrarão em  contato e vocês podem salvar ou fixar a conversa deles.",
  },
  {
    id: 13,
    title: "Posso trabalhar enquanto estudo a kenzie?",
    text: "Não, durante o periodo que consiste no contrato não se deve fazer outros trabalhos paralelos",
  },

  {
    id: 13,
    title: "Quando vou poder trabalhar?",
    text: "O sexto modulo é completamente voltado para o mercado de trabalho.",
  },
  {
    id: 5,
    title: "Posso instalar Dual Boot?",
    text: "Não, não recomendamos a instalação de dual boot para evitar possiveis problemas relativos a perda de dados ou do próprio Sistema operacional do Ubuntu.",
  },
  {
    id: 6,
    title: "Devo instalar Linux(Ubuntu)?",
    text: "Sim, todo o material do curso está voltado para o sistema operacional Ubuntu, não conseguimos da suporte a outro sistema que não seja esse.",
  },
  {
    id: 7,
    title: "Posso usar Windows ou outro sistema operacional?",
    text: "Não, o Ubuntu é o único sistema operacional que a kenzie da suporte durante o curso.",
  },
  {
    id: 8,
    title: "Posso usar outro sistema Unix?",
    text: "Não, o Ubuntu é um sistema Unix otimizado e consistente, por isso é indicado ao uso.",
  },
  {
    id: 9,
    title: "Se eu tiver um Mac, preciso instalar o Ubuntu?",
    text: "Não, não é necessario mudar o sistema padrão do Mac, pois ele também é um Unix-like.",
  },
  {
    id: 10,
    title: "Quantas faltas eu posso ter?",
    text: "É permitido apenas duas faltas não justificadas por bimestre, para justificar",
  },
  {
    id: 11,
    title: "Quantos atrasos eu posso ter?",
    text: "No canvas existe uma nota de frequencia, ela se inicia com 65 pontos, a cada atraso ela diminui 0.33",
  },
  {
    id: 12,
    title: "Como posso responder o bot da daily?",
    text: "Na sala do seu facilitador utilize o comando no chat e responda as perguntas: /kenzie checkin",
  },
  {
    id: 12,
    title:
      "Com quem preciso falar caso precise faltar ou tenha um problema técnico?",
    text: "Qualquer problema de saúde ou problema tecnico, avise o sucesso do aluno e seu facilitador, se puder seus coachs também.",
  },
  {
    id: 12,
    title: "Quais atividades eu preciso entregar?",
    text: "Qualquer aula do canvas que no titulo esteja escrito: 'Entrega'.",
  },
  {
    id: 12,
    title: "Como fazer uma entrega no Canvas?",
    text: "Entrando na página da entrega é necessario clicar no botão de 'Enviar' e mandar o que você precisa",
  },
  {
    id: 12,
    title: "Porquê as atividades tem data de entrega?",
    text: "Apenas para auxiliar vocês sobre qual o dia que é recomendado você estar fazendo aquela atividade, lembrando que o ritmo de estudos são vocês que definem.",
  },
  {
    id: 12,
    title: "Posso adiantar o conteúdo?",
    text: "Sim, você pode fazer tudo ao seu ritmo, as datas do canvas são apenas para auxilio.",
  },
  {
    id: 12,
    title: "Posso enviar entregas atrasadas?",
    text: "Sim, a correção será feita da mesma forma, porém sempre tente manter a frequencia",
  },
  {
    id: 12,
    title: "Posso reenviar uma entrega caso não consiga toda a nota?",
    text: "Sim, apenas lembre de clicar no botão 'reenviar', pois a tarefa precisa ser literalmente reenviada",
  },
  {
    id: 12,
    title: "Posso deixar meu repositório publico?",
    text: "Não, seu repositório deve estar sempre no modo Internal.",
  },

  {
    id: 12,
    title: "Suporte, a quem devo recorrer?",
    text: "Primeiro coach, /pergunta, facilitador e por ultimo instrutor",
  },
  {
    id: 12,
    title: "Quando posso parar para almoçar ou fazer um intervalo?",
    text: "Fora os horários das reuniões as rotinas são definidas por vocês, ou seja, desde que não entre em conflito com uma reunião você pode reservar o horario para o almoço ou breve intervalo.",
  },
  {
    id: 12,
    title: "As demos são gravadas?",
    text: "Sim, todas as demos são gravadas e podem ser acessadas pelo canvas.",
  },
  {
    id: 12,
    title: "As entregas são até meia noite do dia em questão?",
    text: "Sim, até a meia noite do dia marcado, não do proximo dia.",
  },
];
box = document.getElementById("box");
messages.map((element) => {
  details = document.createElement("details");
  summary = document.createElement("summary");
  paragraph = document.createElement("p");

  details.className = "details";
  summary.innerText = element.title;
  summary.className = "summary";
  paragraph.innerText = element.text;
  paragraph.className = "paragraph";

  details.appendChild(summary);
  details.appendChild(paragraph);
  box.appendChild(details);
});
